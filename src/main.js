/**
 * Created by Maggie on 19/05/2017.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import 'normalize.css'; // Note this
require ('../odds.scss');


document.addEventListener('DOMContentLoaded', function() {
    ReactDOM.render(
        React.createElement(App),
        document.getElementById('droc-table')
    );
});