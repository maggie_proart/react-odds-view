/**
 * Created by Maggie on 19/05/2017.
 */
import React from 'react';
var json = require("../data/odds.json");
var bookies = require("../data/bookies.json");
import OddsRace from './components/odds_race.js';




// load json
class App extends React.Component {
    constructor() {
        super();
        this.state = {
            feed: json,
            bookies : bookies.bookie,
        };
    }

    render() {
       return (
            <div>
                {this.state.feed.map((thisEvent,index)=>{
                    return <OddsRace key={index} event={thisEvent}  bookies={this.state.bookies}></OddsRace>
                })}
            </div>
            );
    }
}
export default App

