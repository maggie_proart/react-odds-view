/**
 * Created by Maggie on 23/05/2017.
 */
import React from 'react';


export default class OddsWeather extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            weather: props.weather,
        };
    }

GetElement(the_element){
    var tifOptions = Object.keys(the_element).map(function(key) {
        return <li><span className="title">{key}:</span><span className="data">{the_element[key]?the_element[key]:'NA'}</span></li>
    });
return(tifOptions);
}
    render() {
return   <ul className="droc-weather">
            {
                this.state.weather.map((weatherList, index)=> {
                   return this.GetElement(weatherList);

                })
            }
                </ul>
    }


}