/**
 * Created by Maggie on 31/05/2017.
 */
import React from 'react';
import OddsBookieImage from './odds_bookie_image'

export default class OddCell extends React.Component {
    constructor(props){
        super(props)
        this.state={
            type : props.type ? props.type :'win',
            odd: props.odd,
            bookie: props.bookie,
            show_logo:props.show_logo?props.show_logo:false,
            show_button:props.show_button?props.show_button:false,

        }
    }
    render(){


        let link_button=this.state.show_button ? <a href={this.state.bookie.cta_link} target="_blank" alt={this.state.bookie.title_tag} className="droc-button info droc-bet-link"> bet </a>:'';
        let bookie_logo=this.state.show_logo ?  <OddsBookieImage image_mode=""  item={this.props.bookie}/> :'';

        let odd =  <span className="odd">-</span>;
        if (this.state.odd){
            odd = <a className="odd" href={this.state.bookie.cta_link}>{this.state.type=='win'? this.state.odd.Win :this.state.odd.Place }</a>
        }
        return <div className="droc-odd">{bookie_logo}{odd}{link_button}</div>
    }
}