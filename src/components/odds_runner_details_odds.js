/**
 * Created by Maggie on 30/05/2017.
 */

import React from 'react';
import OddsRunnerDetails from './odds_runner_details'
import OddsRunnerOddsList from './odds_runner_odds_list'
import OddsAccordionItem from './core/AccordionItem.js';

export default class OddsRunnerDetailsOdds  extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            runner :props.runner,
            bookies: props.bookies,
            responsive_mode :  props.responsive_mode,
            event_type : props.event_type,
            type : props.type
        }
    }

    render(){
        let view = null
        let button = {
            class:"droc-accordion-button primary small-text",
            text:"<span>more odds</span>",
            hoverText:"<span>hide odds</span>"
        }
        if (this.state.responsive_mode === 'lg') {
            view = [
                <OddsRunnerDetails  {...this.props} key={1} runner={this.state.runner} show_floc="true" show_best_odds="false"/>,
                <OddsRunnerOddsList key={2}  odds={this.state.runner.Odds} bookies={this.state.bookies} responsive_mode={this.state.responsive_mode} />

             ]}else {
            view =  <OddsAccordionItem button={button} header={   <OddsRunnerDetails  {...this.props} key={1} runner={this.state.runner} show_floc="true" show_best_odds="false"/>}  body={ <OddsRunnerOddsList key={2}  odds={this.state.runner.Odds} bookies={this.state.bookies} responsive_mode={this.state.responsive_mode} />} />
        }

        return <div className="droc-runner-odds">
                    {view}
               </div>
    }

}