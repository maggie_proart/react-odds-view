/**
 * Created by Maggie on 29/05/2017.
 */
import React from 'react';
import OddsBookieImage from './odds_bookie_image'

export default class OddsBookiesImageList extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            image_mode : props.image_mode? props.image_mode:'v',
            bookies : props.bookies
        };
    }
    render(){
let $this = this;
       const images =  Object.keys(this.props.bookies).map(function(keyName, keyIndex) {
            return <OddsBookieImage image_mode={$this.state.image_mode} key={keyIndex} item={$this.props.bookies[keyName]}/>
        })


        return <div className="droc-bookies-image-list">
            {images}
        </div>
    }
}