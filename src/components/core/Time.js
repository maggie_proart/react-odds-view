/**
 * Created by Maggie on 29/05/2017.
 */
import React from 'react';
import moment from 'moment';

export default class Time extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        return <time className="droc-time"><i></i>{moment(this.props.time,"X").format(this.props.format)}</time>
    }
}