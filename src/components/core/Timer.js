/**
 * Created by Maggie on 23/05/2017.
 */
import React from 'react';
import moment from 'moment';


export default class OddsTimer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: props.time,
            timeRemaining:  moment(props.time,"X").format(' h:mm a'),
            raceOver: false

        };
    }

    componentDidMount() {
        this.tick();
        this.interval = setInterval(this.tick.bind(this), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    tick() {
        let now = moment.utc().unix();
        let td = (this.state.time - now); //time difference )

        switch (true) {
            case (td < 0):

                this.setState({timeRemaining: moment.unix(this.state.time).format("HH:mm a" ), raceOver:true})
                break;
            case (td < 61):
                this.setState({timeRemaining: td +"sec"});
                break;
            case (td < 3600):
                this.setState({timeRemaining: Math.floor(td/60)+" min"});
                break;
            case (td > 3600):
                this.setState({timeRemaining: Math.floor(td/3600)+"hr "+  Math.floor((td % (60 * 60)) / 60) + "min"});
                break;
        }

    }

    render() {
        return <span className={this.state.raceOver ? 'droc-timer danger' : 'droc-timer'}> {this.state.timeRemaining} </span>

    }


}