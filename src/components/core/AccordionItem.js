import React from 'react'
import AccordionBody from './AccordionBody.js';
class OddsAccordionItem extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            show_body:'hide',
            button: this.props.button ? this.props.button : null,
            buttonToggle: ''
        }

    }
    ToggleAccordionBody (e){
        let toggleto= this.state.show_body=='hide' ? 'show':'hide';
        let button_toggle= this.state.buttonToggle=='' ? 'open':'';
        this.setState ({show_body: toggleto,buttonToggle: button_toggle })
        console.log(this.state)


    }

    render(){
        let body='';
        let button = this.state.button ? <button onClick={ this.ToggleAccordionBody.bind(this)} className= {this.state.buttonToggle +" " +( this.state.button.class ? this.state.button.class:'droc-button')} dangerouslySetInnerHTML={{__html:this.state.buttonToggle=='open'?this.state.button.hoverText:this.state.button.text }}></button>:'';
        if (this.state.show_body=='show') {
            body = <div className="droc-accordion-body">
                {this.props.body}
            </div>
        }
        return (
           <div className="droc-accordion-item">
              <div className="droc-accordion-header" >
                  {this.props.header}
                  {button}
              </div>
               {body}

            </div>
        )
    }
}

export default OddsAccordionItem;