/**
 * Created by Maggie on 24/05/2017.
 */
import React from 'react';

export default class OddsWinPlaceButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'win',
        };
    }
    toggleOddsType(e){
        console.log(e.currentTarget.dataset.vale);
        this.setState({type:e.currentTarget.dataset.vale});
    }
    render() {
        return  <div className="droc-toggle">
            <button className= {this.state.type == 'place' ? 'droc-button active' : 'droc-button'} onClick={this.toggleOddsType.bind(this)} data-vale="place">Place</button>
            <button className= {this.state.type == 'win' ? 'droc-button active' : 'droc-button'} onClick={this.toggleOddsType.bind(this)} data-vale="win">Win</button>
        </div>
    }
    }