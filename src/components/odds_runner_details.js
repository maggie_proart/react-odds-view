/**
 * Created by Maggie on 30/05/2017.
 */
import React from 'react';
import OddStats from './odd_stats'
import OddSilk from './odd_silk'

export default class OddsRunnerDetails extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            runner : props.runner,
            bookies: props.bookies,
            show_floc : props.show_floc ? props.show_floc : true,
            show_best_odds: props.show_best_odds ? props.show_best_odds : true,
            event_type : props.event_type,
            type:props.type
        }
    }

    render(){

         let OddsStats = (this.state.show_floc || this.state.show_best_odds) ? <OddStats {...this.props} /> : '';
        return <div className="droc-runner-details">
                <div className="droc-runner">
                    <OddSilk event_type={this.state.event_type} number={this.state.runner.RunnerNumber}/>
                    <div>
                    <span className="droc-runner-num">{this.state.runner.RunnerNumber}.</span> <strong className="droc-runner-title">{this.state.runner.RunnerName}</strong>
                    <div className="droc-jockey">
                        {this.state.runner.Jockey==''?this.state.runner.Trainer : this.state.runner.Jockey} {this.state.runner.Weight > 0 ? this.state.runner.Weight : ''}
                    </div>
                        </div>
                </div>
              {OddsStats}
            </div>

    }

}