/**
 * Created by Maggie on 22/05/2017.
 */
import React from 'react';
import OddsWeather from './odds_weather.js';
import OddsWinPlaceButton from './odds_winplace_button';
import OddsTimer from './core/Timer';
export default class OddsHeader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            event: props.event,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({responsive_mode: nextProps.responsive_mode});
    }

    getWeatherElements() {
        let weather = [{"Weather": this.state.event.Weather}, {"Track Condition": this.state.event.TrackCondition}];

        if (this.state.event.WeatherDetail) {
            weather = [].concat(weather, this.state.event.WeatherDetail);
        }

        return weather;
    }

    render() {

        return <div className="droc_header">
            <div className="droc_meeting">

                {this.props.responsive_mode == "lg" &&
                <div className="droc-links">
                    { <OddsWinPlaceButton/>}
                    &nbsp;&nbsp;&nbsp;<a href={"#"+this.state.event.PublicId}  className="droc-button">All Races</a>


                </div>}
                <div className="droc-header-title">
                    <h2>{this.state.event.Meeting} - Race {this.state.event.RaceNumber}</h2>
                    <h3>{this.state.event.Name}</h3>
                </div>
            </div>

            {this.props.responsive_mode == "lg" ? (
                <OddsWeather weather={this.getWeatherElements()}/>
            ) : (
                <OddsTimer time={this.state.event.StartTimeStamp} />
            )}

        </div>
    }
}
