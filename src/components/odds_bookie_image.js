/**
 * Created by Maggie on 29/05/2017.
 */
import React from 'react';


export default class OddsBookieImage extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            image_mode : props.image_mode? '-'+props.image_mode:'',
            item : props.item
        };
    }
    render(){
        const path = "http://brad1.race.media/wp-content/plugins/Odds-Boss/images/bookies/";
        let item = this.state.item;
        return <div  className={'droc-bookies-image ' + item.code} >
            <a href={item.cta_link} alt={item.alt_tag}>
                 <img src={path+item.image+this.state.image_mode+'.svg'} title={item.title_tag+" "+item.bonus_text} alt={item.alt_tag+" "+item.bonus_text} />
            </a>
        </div>
    }
}