/**
 * Created by Maggie on 1/06/2017.
 */
import React from 'react'
import * as Path from './const'

export default class OddSilk extends React.Component {
    constructor(props){
        super(props)
    }
    Dogs(){ return (<img src={Path.imgPath+'dogs/dogs_'+(this.props.number)+'.svg'} />)}
    Harness(){ return ( <img src={Path.imgPath+'silks/harness_default.svg'} />) }
    Races(){ return ( <img src={Path.imgPath+'silks/jockey_default.svg'} /> ) }

    render() {

        return(
            <span className="droc-silk">
                {{
                    'G': this.Dogs(),
                    'H': this.Harness(),
                    'R': this.Races(),
                    null: ''

                }[this.props.event_type]}
            </span>    )
    }
}