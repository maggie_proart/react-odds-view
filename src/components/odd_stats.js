/**
 * Created by Maggie on 31/05/2017.
 */
import React from 'react';


export default class OddStats extends React.Component {
    constructor(props){
        super(props)

    }
    render(){
        let floc = this.props.show_floc ? <div className="droc-odds-fluc">fluc</div> : '';
        let bestodd = this.props.show_best_odds ? <div className="droc-best-odds"><span className="droc-best-odds-price">$18</span>&nbsp;<span className="droc-best-odds-percentage">12%</span><div className="droc-best-bookie">bookie name</div></div> : '';
        //let floc
        return <div className="droc-odds-stats">{floc} {bestodd}  </div>
    }
}