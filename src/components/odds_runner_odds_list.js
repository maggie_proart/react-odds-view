/**
 * Created by Maggie on 30/05/2017.
 */
import React from 'react'
import OddCell from './odd_cell'

export default class OddsRunnerOddsList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            odds :props.odds,
            bookies: props.bookies,
            responsive_mode :  props.responsive_mode,
            type : props.type ? props.type :'win',
        }

    }
    render() {
        let $this= this;
        let button = (this.state.responsive_mode=='sm')? true : false;
        let image = (this.state.responsive_mode=='sm')? true : false;
        const bookiesOdds =  Object.keys(this.props.bookies).map(function(keyName, keyIndex) {
            let thisOdds = $this.state.odds.filter((thisOdd)=>{return thisOdd.Bookie === keyName});
            thisOdds = thisOdds.length > 0 ?thisOdds[0]: null;
console.log(button);
            return <OddCell key={keyIndex} odd={thisOdds} type={$this.state.type} show_logo={image} show_button={button} bookie={$this.props.bookies[keyName]}/>

        })


        return <div className="droc-runner-odds-list">
            {bookiesOdds}
        </div>

    }
}