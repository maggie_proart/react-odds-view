/**
 * Created by Maggie on 22/05/2017.
 */
import React from 'react';
import OddsHeader from './odds_header.js';
import OddsDetails from './odds_details.js';
import OddsList from './odds_list.js';
import {getBreakpointFromWidth} from '../utils';
import OddsAccordionItem from './core/AccordionItem.js';
const breakpoints= {lg: 1024,  sm: 0};

export default class OddsRace extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            event: props.event,
            bookies : props.bookies,
            responsive_mode :  getBreakpointFromWidth(breakpoints),
            type: 'win'
        };
    }
    componentWillMount () {
        this.updateResponsive.bind(this);
        window.addEventListener("resize", this.updateResponsive.bind(this));
    }
    updateResponsive() {
        let current_breakpoint = getBreakpointFromWidth(breakpoints);
        if(this.state.responsive_mode!=current_breakpoint){
            this.setState({responsive_mode:current_breakpoint});

        }

    }

    render() {
        let body = [<OddsDetails key={1} event={this.props.event} responsive_mode={this.state.responsive_mode} bookies={this.state.bookies}/>,<OddsList key={2} type={this.state.type} event_type={this.state.event.Type} responsive_mode= {this.state.responsive_mode}  runners={this.state.event.Runners} bookies={this.state.bookies} />]
        let header = <OddsHeader event={this.props.event}  responsive_mode={this.state.responsive_mode}/>
        let button = {
            class:"droc-accordion-button",
            text:"<i class='arrow-down'></i>",
            hoverText:"<i class='arrow-up'></i>"
        }
      return  <div className="droc-race">
          { this.state.responsive_mode == "sm" ? (

              <OddsAccordionItem  button={button}  body={body}  header={header} />
          ) : (
              <div>
                  {header}
                  {body}
              </div>
          )}

            </div>
    }
}
