/**
 * Created by Maggie on 22/05/2017.
 */
import React from 'react';
import OddsTimeDistance from './odd_time_distance'
import OddsWinPlaceButton from './odds_winplace_button';
import OddsBookiesImageList from './odds_bookies_image_list';

export default class OddsDetails extends React.Component {
    constructor(props) {
        super(props);


    }


    render() {

        return <div className="droc-odds-details">
            <OddsTimeDistance time={this.props.event.StartTimeStamp} distance={this.props.event.Distance}/>
            {this.props.responsive_mode == "sm" ? (
                <OddsWinPlaceButton/>) : (
               <OddsBookiesImageList bookies={this.props.bookies} image_mode="v"/>
            )
            }

        </div>
    }
}
