/**
 * Created by Maggie on 29/05/2017.
 */
import React from 'react';
import Time from './core/Time'

export default class OddsTimeDistance extends React.Component {

    render(){
       return <div className="droc-odds-distance-time">
           <Time time={this.props.time} format='h:mma dddd, MMM d YYYY'/>  <br/>
           <span className="droc-distance"><i></i>{this.props.distance}m</span>
       </div>
    }
}