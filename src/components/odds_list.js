/**
 * Created by Maggie on 29/05/2017.
 */
import React from 'react';
import OddsRunnerDetailsOdds from './odds_runner_details_odds'

export default class OddsRunnersBookies extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            runners: props.runners,
            bookies : props.bookies,
            responsive_mode :  props.responsive_mode,
            type : props.type ? props.type :'win',
            event_type : props.event_type
        };
    }

    render (){

        const runners = this.state.runners.map((thisRunner,index)=>{

            return <OddsRunnerDetailsOdds type={this.state.type} event_type={this.state.event_type}  key={index} runner={thisRunner} bookies={this.state.bookies} responsive_mode={this.state.responsive_mode}/>
        });

        return <div className="droc-odd-list">{runners}</div>
    }
}